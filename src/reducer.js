import {
    TOGGLE_COMPLETION,
    CLEAR_COMPLETED,
    ADD_TODO,
    DELETE_TODO
} from "./actions";

const initalState = {
  todos: [],
  value: ""
};

const reducer = (state = initalState, action) => {
  switch (action.type) {
    case TOGGLE_COMPLETION:{
        let newArr = [];
        for(let i = 0; i < state.todos.length; i++){
            if(state.todos[i].id === action.index){
                let toggleThing = state.todos[i];
                toggleThing.completed = !toggleThing.completed;
                newArr.push(toggleThing);
            }
            else
                newArr.push(state.todos[i]);
        }
        console.log(newArr);
        return { todos: newArr };
    }
    case CLEAR_COMPLETED:
      let arr =[];
      for (let i = 0; i < state.todos.length; i ++){
          if(state.todos[i].completed !== true)
            arr.push(state.todos[i]);
      }
      return { todos: arr };

    case ADD_TODO:
        let arr2 = state.todos;
        arr2.push(action.newTodo);
        return { todos: arr2 };
    case DELETE_TODO:
        let delArr = [];
        for(let i = 0; i < state.todos.length; i++){
            if(state.todos[i].id === action.index){
            }
            else
            delArr.push(state.todos[i]);
        }
        console.log(delArr);
        return { todos: delArr };
    default:
      return state;
  }
};

export default reducer;
