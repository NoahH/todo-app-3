import React, { Component } from "react";
import "./index.css";
import TodoList from "./TodoList";
import {
  Route,
  NavLink,
} from "react-router-dom";
import { connect } from 'react-redux'
import { clearCompleted, addTodo } from "./actions";


class App extends Component {
  state = {
    todos: [],
    inputText: ""
  };

  submitBox = (event) => {
    if(event.key === "Enter" && this.state.inputText.length !== 0){
      let index = -1;
      for(let i = 0, chooseNew = false; index === -1; i ++, chooseNew = false){
        for(let j = 0; j < this.props.todos.length; j ++){
          if(this.props.todos[j].id === i){
            chooseNew = true;
          }
        }
        if(chooseNew === false){
          index = i;
        }
      }
      this.props.addTodo(this.state.inputText, index);
      this.setState({ inputText: "" });
      document.getElementsByClassName("new-todo")[0].value = "";
    }
    else{
      this.setState({todos: this.state.todos, inputText: event.target.value});
    }
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyUp={this.submitBox}
          />
        </header>

        <Route 
          exact 
          path="/active"
          render = {() => (
            <TodoList todos={this.props.todos.filter(todo => todo.completed === false)} whatToRender = "active"/>
          )}>
        </Route>
        <Route 
          exact 
          path="/completed"
          render = {() => (
            <TodoList todos={this.props.todos.filter(todo => todo.completed === true)} whatToRender = "completed"/>
          )}>
        </Route>
        <Route 
          exact 
          path="/"
          render = {() => (
            <TodoList todos={this.props.todos}whatToRender = ""/>
          )}>
        </Route>
          

        <footer className="footer">
          <span className="todo-count">
            <strong>
              {(() => {
                let incomplete = this.props.todos.filter(
                  todo => todo.completed !== true
                );
                return incomplete.length;
              })()}
            </strong>{" "}
            item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick = {this.props.clearCompleted}>Clear completed</button>
        </footer>
      </section>
    );
  }
}
const mapStateToProps = state => {
  return {
    todos: state.todos
  }
}

const mapDispatchToProps = {
  clearCompleted,
  addTodo
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
