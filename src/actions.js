
export const TOGGLE_COMPLETION = "TOGGLE_TODO";
export const CLEAR_COMPLETED = "CLEAR_COMPLETED";
export const ADD_TODO = "ADD_TODO";
export const DELETE_TODO = "DELETE_TODO";

export const toggleCompletion = index => {
  return {
    type: TOGGLE_COMPLETION,
    index: index
  };
};
export const clearCompleted = () => {
  return {
    type: CLEAR_COMPLETED
  };
};
export const addTodo = (todoTitle, index) => {
  const newTodo = {
    userId: 1,
    id: index,
    title: todoTitle,
    completed: false
  };
  return {
    type: ADD_TODO,
    newTodo: newTodo
  };
};
export const deleteTodo = index => {
  return {
    type: DELETE_TODO,
    index: index
  };
};
