
import React, { Component } from "react";
import { toggleCompletion, deleteTodo } from "./actions";
import { connect } from "react-redux";

class TodoItem extends Component {
  render() {
    return (
      <li identifier = {this.props.identifier} className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onClick = {event => this.props.toggleCompletion(this.props.identifier)}
            identifier = {this.props.identifier}
            readOnly
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick = {event => this.props.deleteTodo(this.props.identifier)}/>
        </div>
      </li>
    );
  }
}
const mapDispatchToProps = {
  toggleCompletion,
  deleteTodo
};

export default connect(
  null,
  mapDispatchToProps
)(TodoItem);
